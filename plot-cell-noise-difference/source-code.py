import os, json, re, ROOT, sys, traceback
from connectors import Connectors

from TileCalibBlobPython import TileCellTools


tPARAMETERS = {"main_path": "/afs/cern.ch/user/y/yabulait/public/noise/cell",
               "file_name": "ReadCellNoiseFromCoolCompare_summary.txt",
               "nruns": 5
              }

runs = [irun for irun in os.listdir(tPARAMETERS["main_path"]) if irun.isdigit()]
nruns = min(tPARAMETERS["nruns"], len(runs))
runs = runs[:nruns]

def get_relative_diff_from_file(fileName):
  lines=None
  #Read data from a file. The file contains cell_number, gain, cell noise value, and difference.
  with open(fileName) as f:
    lines = f.readlines()
    #print lines[0]
  lines = [line.lstrip(' ').rstrip(' \n').replace('\t','').split(' ') for line in lines]
  #print lines[483:]
  lines = [filter(None, line) for line in lines]

  #Data is in [cell_number, gain, relative_difference]
  diff_re = [ [int(iLine[0]), int(iLine[1]), float(iLine[9])] for iLine in lines]
  #print diff_re
  return diff_re

hashMgr=TileCellTools.TileCellHashMgr()
def get_name(cell_number):
  if cell_number <0 or cell_number> 5183:
    #print("Error: Invalied Cell Number. %s"%cell_number)
    return None

  partition, cellID = hashMgr.getNames(cell_number)
  return [ partition[:3], int(partition[3:]), cellID ]

#store all data
data = {}

partitions = {}
partitions['LBC']=['A-%s'%i for i in range(1,11)] + ['B-%s'%i for i in range(1,10)] + ['D-%s'%i for i in range(4)]
partitions['LBA']=['A+%s'%i for i in range(1,11)] + ['B+%s'%i for i in range(1,10)] + ['D+%s'%i for i in range(4)]+['D*0']
partitions['EBC']=['A-%s'%i for i in range(12,17)]+['B-%s'%i for i in range(11,16)] +['C-10']+ ['D-%s'%i for i in range(4,7)]+['E-%s'%i for i in range(1,5)]+['e4E-1', 'spC-10', 'spD-4', 'spD-40', 'spE-1', 'mbE-1']
partitions['EBA']=['A+%s'%i for i in range(12,17)]+['B+%s'%i for i in range(11,16)] +['C+10']+ ['D+%s'%i for i in range(4,7)]+['E+%s'%i for i in range(1,5)]+['e4E+1', 'spC+10', 'spD+4', 'spD+40', 'spE+1', 'mbE+1']

#fill data with zero
zero_data ={}
for ipart in partitions.keys():
    cells = partitions[ipart]
    zero_data[ipart] = {}
    for imod in range(1,65):
        zero_data[ipart][imod]={}
        for icell in cells:
            zero_data[ipart][imod][icell] = { 0: 0.0, 1: 0.0, 2:0.0, 3:0.0} # 0-3 are gains


for run in runs:
    full_path = tPARAMETERS["main_path"]+'/'+run+'/'+tPARAMETERS["file_name"]
    try:
        current_file = open(full_path)
        current_file.close()
    except IOError:
        print ('File is not accessible in %s'%run)
        continue
    data_of_this_run = get_relative_diff_from_file(full_path)
    #iterate all cells and convert cell number (0-5183) to cell name(ID)
    data[run] = zero_data
    #print data
    for icell in data_of_this_run:
        icell_number = icell[0]
        gain = icell[1]
        rel_diff = icell[2]
        cell_geo = get_name(icell_number)
        if cell_geo is None: continue
        partition = cell_geo[0]
        module = cell_geo[1]
        cell_name = cell_geo[2]
        #print icell_number, gain, partition, module, cell_name
        data[run][partition][module][cell_name][gain] = rel_diff


old_FLOAT_REPR = json.encoder.FLOAT_REPR
old_c_make_encoder = json.encoder.c_make_encoder
json.encoder.FLOAT_REPR = lambda o: '%.5g' % o
json.encoder.c_make_encoder = None
    
data = json.dumps(data)

json.encoder.FLOAT_REPR = old_FLOAT_REPR
json.encoder.c_make_encoder = old_c_make_encoder
    
# cleanup
globs_to_del = ['parameters', 'zero_data', 'old_FLOAT_REPR', 'old_c_make_encoder', 'lines', 'f', 'hashMgr']
for gl in globs_to_del:
    if gl in globals(): del globals()[gl]
del gl, globs_to_del
