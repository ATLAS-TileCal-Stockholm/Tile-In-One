import os, json, re, ROOT, sys, traceback
from connectors import Connectors

#tVENDOR = 'tucs' # 'cooldb'

t_params = {
	"nruns": "20",
	"path": "/afs/cern.ch/user/t/tilecali/w0/NoiseCalibArea/results/",
	"prefix_root": "Pedestal",
	"prefix_sqlite": "tile_pedestal",
	"pattern_sqlite": "tile_pedestal_%d.sqlite",
}

# number of historical runs to consider
_nruns = int(t_params['nruns'])
_ped_thresholds = [0.5, 3, 10]
_thresholds = [0.5, -1, -1]

_path = t_params['path']
_prefix_root = t_params['prefix_root']
_prefix_sqlite = t_params['prefix_sqlite']
_pattern_sqlite = t_params['pattern_sqlite']

_filed = None
_files = None
_noise_data = None
    
# make sure we can clean up on exception
try:
    class Hi:
        def __init__(self, nbins, xmin, xmax):
            self.m_nbins = nbins
            self.m_xmin = float(xmin)
            self.m_xmax = float(xmax)
            self.m_bins = [0] * (nbins + 2)
        
        def fill(self, x):
            bin = None
            if x < self.m_xmin: bin = 0
            elif x >= self.m_xmax: bin = -1
            else: bin = 1 + int(float(self.m_nbins)*(x - self.m_xmin)/(self.m_xmax - self.m_xmin))
            if bin: self.m_bins[bin] += 1.0
        
        def get_data(self):
            xstep = (self.m_xmax - self.m_xmin)/float(self.m_nbins)
            return [[round(self.m_xmin + xstep*float(bin), 5), self.m_bins[bin]] for bin in range(1, self.m_nbins + 1)]
        
        # Move overflow to last bin
        def move_overflow(self):
            self.m_bins[self.m_nbins] += self.m_bins[self.m_nbins + 1]
            self.m_bins[self.m_nbins + 1] = 0
        
    _file_map = {}
    _files = []
    _noise_data = {}

    for runnr_str in os.listdir(_path):
        try:
            runnr = int(runnr_str)
            lfile = os.path.join("%d" % runnr, "%s_%d.root" % (_prefix_root, runnr))
            file_name = os.path.join(_path, lfile)
            if os.path.isfile(file_name):
                _file_map[runnr] = {'run_number':runnr, 'file_name':lfile}
        except ValueError:
            pass
    
    _run_nrs = sorted(_file_map, reverse=True)
    for runidx in range(min(len(_run_nrs), _nruns)):
        filedata = _file_map[_run_nrs[runidx]]
        rfile = ROOT.TFile(os.path.join(_path, filedata['file_name']))
        txt = rfile.Get('PedestalValues')
        _filed = json.loads(txt.GetTitle())
        if _filed['date']:
            filedata['info'] = {'date': int(_filed['date']*1000), 
                                'lg_thresholds':  _filed['lg_thresholds'],
                                'hg_thresholds':  _filed['hg_thresholds'],
                                'attributes' : _filed['attributes'],
                                'sqlite_file': os.path.join(_path, "%d" % filedata['run_number'], _pattern_sqlite % filedata['run_number'])}
            _lg_hi = Hi(50, 0, 5)
            _hg_hi = Hi(50, 0, 5)
            _files.append(filedata)
        
            for part in _filed['data']:
                if not part in _noise_data: _noise_data[part] = {'diffs':[]}
                diffs = [0]*len(_ped_thresholds)
                _noise_data[part]['diffs'].append(diffs)
                for mod in _filed['data'][part]:
                    if not mod in _noise_data[part]: _noise_data[part][mod] = {}
                    for chan in _filed['data'][part][mod]:
                        if not chan in _noise_data[part][mod]: _noise_data[part][mod][chan] = {}
                        for gain in _filed['data'][part][mod][chan]:
                            d = _filed['data'][part][mod][chan][gain]
                            ped_diff = abs(d[0] - d[1])
                            # count ADC:s above thresholds
                            if not d[-1]:
                                for t in range(len(_ped_thresholds)):
                                    if ped_diff > _ped_thresholds[t]: diffs[t] += 1
                            if gain == "0": _lg_hi.fill(ped_diff)
                            else: _hg_hi.fill(ped_diff)
                            
                            if not gain in _noise_data[part][mod][chan]:
                                _noise_data[part][mod][chan][gain] = [d]
                            else:
                                _noise_data[part][mod][chan][gain].append(d)
            _lg_hi.move_overflow()
            _hg_hi.move_overflow()
            filedata['ped_diff_lg'] = _lg_hi.get_data()
            filedata['ped_diff_hg'] = _hg_hi.get_data()
    
    # remove data for 'good' ADC:s
    for part in _noise_data:
        if part == 'diffs': continue
        mods_to_del = []
        for mod in _noise_data[part]:
            if mod == 'diffs': continue
            chans_to_del = []
            for chan in _noise_data[part][mod]:
                to_del = []
                for gain in _noise_data[part][mod][chan]:
                    is_good = True
                    for fdata in _noise_data[part][mod][chan][gain]:
                        # TODO: checks for noise
                        if abs(fdata[0] - fdata[1]) > _thresholds[0]: is_good = False
                    if is_good: to_del.append(gain)
                for gain in to_del:
                    del _noise_data[part][mod][chan][gain]
                if len(_noise_data[part][mod][chan]) == 0: chans_to_del.append(chan)
            for chan in chans_to_del: del _noise_data[part][mod][chan]
            if len(_noise_data[part][mod]) == 0: mods_to_del.append(mod)
        for mod in mods_to_del: del _noise_data[part][mod]

    old_FLOAT_REPR = json.encoder.FLOAT_REPR
    old_c_make_encoder = json.encoder.c_make_encoder
    json.encoder.FLOAT_REPR = lambda o: '%.5g' % o
    json.encoder.c_make_encoder = None
    
    files_json = json.dumps(_files)
    noise_data_json = json.dumps(_noise_data)
    
    json.encoder.FLOAT_REPR = old_FLOAT_REPR
    json.encoder.c_make_encoder = old_c_make_encoder
    
    # cleanup
    globs_to_del = ['t', '_file_map', '_files', '_filed', '_run_nrs', '_noise_data', 
                    '_lg_hi', '_hg_hi', 'diffs', 'filedata', 'is_good', 'to_del', 'chans_to_del',
                    'old_c_make_encoder', 'old_FLOAT_REPR', '_prefix_root', '_prefix_sqlite', '_pattern_sqlite',
                    'file_name', 'lfile', 'txt', 'rfile', 'Hi', 'fdata', 'mods_to_del', '_run_nrs']
    for gl in globs_to_del:
        if gl in globals(): del globals()[gl]
    del gl, globs_to_del
except:
    print "Exception:"
    traceback.print_exc(file=sys.stdout)
    if _filed: del _filed
    if _files: del _files
    if _noise_data: del _noise_data
    

